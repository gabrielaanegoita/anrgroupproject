#Import libraries
from tkinter import *
from tkinter import ttk

def calculate(*args):
    try:
        ml_value = float(ml.get())                  #Get the quantity introduced
        rotations.set(ml_value / .52)               #Calculate the rotations
        code.set((7.5 * ml_value) / .52)            #Calculate the G-code
    except ValueError:
        pass

root = Tk()                                         #Create the frame
root.title("Rotations Calculator G-code Generator") #Title of the frame

mainframe = ttk.Frame(root, padding="3 3 12 12")    #Set the parameters of the frame
mainframe.grid(column=0, row=0, sticky=(N, W, E, S))
root.columnconfigure(0, weight=1)
root.rowconfigure(0, weight=1)

#Declare the variables
ml = StringVar()
rotations = StringVar()
code = StringVar()

#Position of the varibles in the frame
ttk.Label(mainframe, text="Quantity:").grid(column=1, row=1, sticky=E)
ml_entry = ttk.Entry(mainframe, width=7, textvariable=ml)
ml_entry.grid(column=2, row=1, sticky=(W, E))
ttk.Label(mainframe, text="ml").grid(column=3, row=1, sticky=W)

ttk.Button(mainframe, text="Calculate", command=calculate).grid(column=3, row=4, sticky=W)
ttk.Label(mainframe, textvariable=rotations).grid(column=2, row=2, sticky=(W, E))
ttk.Label(mainframe, textvariable=code).grid(column=3, row=3, sticky=(W))

ttk.Label(mainframe, text="ml").grid(column=3, row=1, sticky=W)
ttk.Label(mainframe, text="require").grid(column=1, row=2, sticky=E)
ttk.Label(mainframe, text="rotations of gear").grid(column=3, row=2, sticky=W)
ttk.Label(mainframe, text="G-code").grid(column=1, row=3, sticky=E)
ttk.Label(mainframe, text="E1").grid(column=2, row=3, sticky=(W, E))
text = Text(root, width=65, height=25)
text.grid(row=4, column=0, columnspan=4)

for child in mainframe.winfo_children(): child.grid_configure(padx=5, pady=5)

ml_entry.focus()
root.bind('<Return>', calculate)

root.mainloop()
